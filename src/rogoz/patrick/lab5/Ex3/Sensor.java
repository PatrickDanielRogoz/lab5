package rogoz.patrick.lab5.Ex3;

import java.util.ArrayList;
import java.util.Random;

abstract class Sensor {
    String location;
    public Sensor(String location){
        this.location=location;
    }

    public String getLocation() {
        return location;
    }
    abstract int readValue();
}

class TemperatureSensor extends Sensor{
    Random random=new Random();
    int value=random.nextInt(101);

    public TemperatureSensor(String location) {
        super(location);
    }

    @Override
    public int readValue(){
        return value;
    }
}

class LightSensor extends Sensor{
    Random random=new Random();
    int value=random.nextInt(101);
    public LightSensor(String location) {
        super(location);

    }

    @Override
    public int readValue(){
        return value;
    }
}
class Controller{
    //private ArrayList<TemperatureSensor>tempSensor=new ArrayList<TemperatureSensor>(20);
    // private ArrayList<LightSensor>lightSensor=new ArrayList<LightSensor>(20);
    public void readDisplay(){
        for (int i=0;i<20;i++) {
            TemperatureSensor t = new TemperatureSensor("Home");
            // tempSensor.add(t);
            LightSensor l=new LightSensor("Home");
            // lightSensor.add(l);
            System.out.println(i+1+". Minute: Temperature= "+t.readValue()+" Light= "+l.readValue());
        }
    }

}