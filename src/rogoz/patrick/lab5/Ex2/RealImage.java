package rogoz.patrick.lab5.Ex2;

interface Image { void display(); }

public class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display() {
        System.out.println("Displaying " + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}

class ProxyImage implements Image{

    private RealImage realImage;
    private String fileName;
    private RotatedImage rotatedImage;
    int choose;

    public ProxyImage(String fileName,int choose){
        this.fileName = fileName;
        this.choose=choose;
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        if(rotatedImage == null){
            rotatedImage = new RotatedImage(fileName);
        }
        if (choose==0)
            realImage.display();
        else rotatedImage.display();
    }
}

class RotatedImage implements Image{
    private String filename;
    public RotatedImage(String filename){
        this.filename=filename;
    }
    @Override
    public void display() {
        System.out.println("Display rotated "+filename);
    }
}